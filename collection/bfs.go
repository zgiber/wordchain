package collection

// BFSTrace is a customized version of the BFS algorithm
// It stops when endID is reached, and returns the trace from
// start to end as a slice of nodeIDs.
func BFSTrace(c *Collection, startID, endID string) ([]string, error) {
	if c.graph.GetNode(startID) == nil {
		return nil, nil
	}

	q := []string{startID}
	visited := make(map[string]bool)
	visited[startID] = true

	// while Q is not empty:
	for len(q) != 0 {

		u := q[0]
		q = q[1:len(q):len(q)]

		// for each vertex adjacent to parentNode:
		parentNode := c.graph.GetNode(u)
		children := parentNode.GetChildren()
		for _, childNode := range children {

			// if childNode is not visited yet:
			if _, ok := visited[childNode.ID]; !ok {
				q = append(q, childNode.ID)       // Q.push
				visited[childNode.ID] = true      // label as visited
				childNode.bestparent = parentNode //
			}

			// return with search path if there is a match
			if childNode.ID == endID {
				return traceToSource(childNode), nil
			}

		}
		parents := c.graph.GetNode(u).GetParents()
		for _, parentNode := range parents {
			// if w is not visited yet:
			if _, ok := visited[parentNode.ID]; !ok {
				q = append(q, parentNode.ID)  // Q.push
				visited[parentNode.ID] = true // label as visited
			}
		}
	}

	// return empty slice if there is no match
	// so we can encode it to the stdOut
	return []string{}, nil
}

func traceToSource(node *Node) []string {
	var result []string
	for {
		result = append(result, node.ID)
		node = node.bestparent
		if node == nil {
			return reverse(result)
		}
	}
}

func reverse(src []string) []string {
	var result []string
	for i := len(src) - 1; i >= 0; i-- {
		result = append(result, src[i])
	}
	return result
}
