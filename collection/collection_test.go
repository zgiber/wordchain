package collection

import "testing"

func TestCollection(t *testing.T) {
	sample := []string{
		"aa", "bb", "ab", "aa",
	}

	expect := &Collection{
		wordGroups: map[string][]string{
			".a": []string{"aa"},
			"a.": []string{"aa", "ab"},
			".b": []string{"bb", "ab"},
			"b.": []string{"bb"},
		},
	}

	c := NewCollection()
	if err := c.SetDictionary(sample); err != nil {
		t.Fatal(err)
	}

	if !collectionIsEqual(c, expect) {
		t.Fatalf("expected:\n%+v\nreceived:\n%+v\n", expect, c)
	}

}

func collectionIsEqual(c1, c2 *Collection) bool {
	for wordGroup, wordList := range c1.wordGroups {
		expectWordList, ok := c2.wordGroups[wordGroup]
		if !ok {
			return false
		}

		// fmt.Println("WORDLIST", wordList, "EXP", expectWordList)
		if !wordListIsEqual(wordList, expectWordList) {
			return false
		}
	}

	return true
}

func wordListIsEqual(l1, l2 []string) bool {
	for i, word := range l1 {
		if l2[i] != word {
			return false
		}
	}

	return true
}
