package collection

import (
	"errors"
	"strings"
	"sync"
)

// Graph represents a simple unweighted
// graph with nodes and edges
type Graph struct {
	nLock sync.RWMutex
	eLock sync.RWMutex
	nodes map[string]*Node
	edges map[string]*edge
}

// Node is a vertex in the graph
// with a special bestparent field
// for being used in our search
type Node struct {
	pLock      sync.RWMutex
	cLock      sync.RWMutex
	ID         string
	Value      interface{}
	bestparent *Node
	parents    []*Node
	children   []*Node
}

type edge struct {
	id    string
	node1 *Node
	node2 *Node
}

// NewGraph returns an initialized *Graph
func NewGraph() *Graph {
	return &Graph{
		nodes: map[string]*Node{},
		edges: map[string]*edge{},
	}
}

// NewNode returns an initialized *Node
func NewNode(id string, value interface{}) *Node {
	return &Node{
		ID:       id,
		Value:    value,
		parents:  []*Node{},
		children: []*Node{},
	}
}

// AddNode adds a new *Node to the graph
// if there is already a node with the ID
// it is ignored. Returns true if added successfully
func (g *Graph) AddNode(n *Node) bool {
	g.nLock.Lock()
	if _, ok := g.nodes[n.ID]; ok {
		g.nLock.Unlock()
		return false
	}
	g.nodes[n.ID] = n
	g.nLock.Unlock()
	return true
}

// RemoveNode deletes an existing *Node from the
// graph, based on it's ID. Returns true if the
// node existed.
func (g *Graph) RemoveNode(nodeID string) bool {
	g.nLock.Lock()
	if _, ok := g.nodes[nodeID]; !ok {
		g.nLock.Unlock()
		return false
	}

	n := g.nodes[nodeID]
	for _, parentNode := range n.parents {
		parentNode.removeChild(n)
	}

	for _, childNode := range n.children {
		childNode.removeParent(n)
	}

	delete(g.nodes, nodeID)

	g.nLock.Unlock()
	return true
}

// GetNode returns the *Node with the id from the *Graph
// If the node does not exist, it returns nil.
func (g *Graph) GetNode(id string) *Node {
	g.nLock.RLock()
	defer g.nLock.RUnlock()
	return g.nodes[id]
}

// AddEdge creates an edge between nodes in the *Graph
// Node with id 'id1' becomes parent to 'id2'
func (g *Graph) AddEdge(id1, id2 string) error {
	g.eLock.Lock()
	defer g.eLock.Unlock()

	node1 := g.GetNode(id1)
	node2 := g.GetNode(id2)

	if node1 == nil || node2 == nil {
		return errors.New("node can not be nil")
	}

	if node1 == node2 {
		return nil
	}

	id := edgeID(id1, id2)
	if _, ok := g.edges[id]; ok {
		return nil
	}

	node2.parents = append(node2.parents, node1)
	node1.children = append(node1.children, node2)

	g.edges[id] = &edge{
		id:    id,
		node1: node1,
		node2: node2,
	}

	return nil
}

// RemoveEdge deletes an edge between two nodes on the *Graph
func (g *Graph) RemoveEdge(id1, id2 string) error {

	id := edgeID(id1, id2)
	graphEdge, err := g.getEdge(id)
	if err != nil {
		return err
	}

	if graphEdge == nil {
		return nil
	}

	g.eLock.Lock()
	defer g.eLock.Unlock()
	graphEdge.node1.removeChild(graphEdge.node2)
	graphEdge.node2.removeParent(graphEdge.node1)
	delete(g.edges, id)

	return nil
}

func (g *Graph) getEdge(edgeID string) (*edge, error) {
	g.eLock.RLock()
	defer g.eLock.RUnlock()

	if edge, ok := g.edges[edgeID]; ok {
		return edge, nil
	}

	return nil, nil
}

func edgeID(nodeID1, nodeID2 string) string {
	return strings.Join([]string{nodeID1, nodeID2}, "")
}

// GetParents returns a []*Node containing all the parents of the node
func (n *Node) GetParents() []*Node {
	n.pLock.RLock()
	defer n.pLock.RUnlock()
	result := make([]*Node, len(n.parents))
	copy(result, n.parents)
	return result
}

// GetChildren returns a []*Node containing all the children of the node
func (n *Node) GetChildren() []*Node {
	n.cLock.RLock()
	defer n.cLock.RUnlock()
	result := make([]*Node, len(n.children))
	copy(result, n.children)
	return n.children
}

func (n *Node) removeParent(parent *Node) {
	n.pLock.Lock()
	removeFromNodeSlice(&n.parents, parent)
	n.pLock.Unlock()
}

func (n *Node) removeChild(child *Node) {
	n.pLock.Lock()
	removeFromNodeSlice(&n.children, child)
	n.pLock.Unlock()
}

func removeFromNodeSlice(nodes *[]*Node, n *Node) {
	for i := 0; i < len(*nodes); i++ {
		if (*nodes)[i] == n {
			if len(*nodes)-1 == i {
				*nodes = (*nodes)[:i]
				return
			}
			*nodes = append((*nodes)[:i], (*nodes)[i+1:]...)
			i--
		}
	}
}
