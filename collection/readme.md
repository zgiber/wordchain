# collection
--
    import "gitlab.com/zgiber/wordchain/collection"


## Usage

#### func  BFSTrace

```go
func BFSTrace(c *Collection, startID, endID string) ([]string, error)
```
BFSTrace is a customized version of the BFS algorithm It stops when endID is
reached, and returns the trace from start to end as a slice of nodeIDs.

#### type Collection

```go
type Collection struct {
}
```

Collection is a structure with methods to collect and process words for a
search.

#### func  NewCollection

```go
func NewCollection() *Collection
```
NewCollection returns an initialized Collection

#### func (*Collection) SetDictionary

```go
func (c *Collection) SetDictionary(words []string)
```
SetDictionary records the dictionary in the collection. Nodes and graph is
generated from the dictionary

#### type Graph

```go
type Graph struct {
}
```

Graph represents a simple unweighted graph with nodes and edges

#### func  NewGraph

```go
func NewGraph() *Graph
```
NewGraph returns an initialized *Graph

#### func (*Graph) AddEdge

```go
func (g *Graph) AddEdge(id1, id2 string) error
```
AddEdge creates an edge between nodes in the *Graph Node with id 'id1' becomes
parent to 'id2'

#### func (*Graph) AddNode

```go
func (g *Graph) AddNode(n *Node) bool
```
AddNode adds a new *Node to the graph if there is already a node with the ID it
is ignored. Returns true if added successfully

#### func (*Graph) GetNode

```go
func (g *Graph) GetNode(id string) *Node
```
GetNode returns the *Node with the id from the *Graph If the node does not
exist, it returns nil.

#### func (*Graph) RemoveEdge

```go
func (g *Graph) RemoveEdge(id1, id2 string) error
```
RemoveEdge deletes an edge between two nodes on the *Graph

#### func (*Graph) RemoveNode

```go
func (g *Graph) RemoveNode(nodeID string) bool
```
RemoveNode deletes an existing *Node from the graph, based on it's ID. Returns
true if the node existed.

#### type Node

```go
type Node struct {
	ID    string
	Value interface{}
}
```

Node is a vertex in the graph with a special bestparent field for being used in
our search

#### func  NewNode

```go
func NewNode(id string, value interface{}) *Node
```
NewNode returns an initialized *Node

#### func (*Node) GetChildren

```go
func (n *Node) GetChildren() []*Node
```
GetChildren returns a []*Node containing all the children of the node

#### func (*Node) GetParents

```go
func (n *Node) GetParents() []*Node
```
GetParents returns a []*Node containing all the parents of the node
