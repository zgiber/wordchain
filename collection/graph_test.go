package collection

import "testing"

func TestGraph(t *testing.T) {
	graph := NewGraph()
	if graph.nodes == nil || graph.edges == nil {
		t.Fatal("failed to init graph internals")
	}

	nodes := []*Node{
		NewNode("1", "value1"),
		NewNode("2", "value2"),
		NewNode("3", "value3"),
	}

	for _, n := range nodes {
		graph.AddNode(n)
	}

	gotNodes := []*Node{
		graph.GetNode("1"),
		graph.GetNode("2"),
		graph.GetNode("3"),
	}

	for i := 0; i < len(nodes); i++ {
		if !nodeIsEqual(nodes[i], gotNodes[i]) {
			t.Fatal("stored node does not match retrieved node")
		}
	}

	// Creating a full mesh graph between nodes
	// (in our case a triangle for 3 nodes)
	for _, node1 := range nodes {
		for _, node2 := range nodes {
			err := graph.AddEdge(node1.ID, node2.ID)
			if err != nil {
				t.Fatal(err)
			}
		}
	}

	for _, n := range nodes {
		if l := len(n.GetChildren()); l != 2 {
			t.Fatalf("expected 2 children, got %v", l)
		}

		if l := len(n.GetParents()); l != 2 {
			t.Fatalf("expected 2 parents, got %v", l)
		}
	}

	// delete all edges
	for _, node1 := range nodes {
		for _, node2 := range nodes {
			err := graph.RemoveEdge(node1.ID, node2.ID)
			if err != nil {
				t.Fatal(err)
			}
		}
	}

	for _, n := range nodes {
		if l := len(n.GetChildren()); l != 0 {
			t.Fatalf("expected 0 children, got %v", l)
		}

		if l := len(n.GetParents()); l != 0 {
			t.Fatalf("expected 0 children, got %v", l)
		}
	}

	for _, n := range nodes {
		graph.RemoveNode(n.ID)
	}

	if graph.RemoveNode("1") {
		t.Fatal("returned true when deleting non existing node")
	}

	if len(graph.nodes) > 0 {
		t.Fatal("failed to delete nodes")
	}

}

func nodeIsEqual(node1, node2 *Node) bool {
	if node1.ID != node2.ID ||
		node1.Value != node2.Value {
		return false
	}
	return true
}
