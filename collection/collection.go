package collection

// Collection is a structure with methods to collect and
// process words for a search.
type Collection struct {
	wordGroups map[string][]string
	graph      *Graph
}

// NewCollection returns an initialized Collection
func NewCollection() *Collection {
	return &Collection{
		graph:      NewGraph(),
		wordGroups: map[string][]string{},
	}
}

// SetDictionary records the dictionary in the collection.
// Nodes and graph is generated from the dictionary
func (c *Collection) SetDictionary(words []string) error {

	for _, word := range words {
		if !c.graph.AddNode(NewNode(word, word)) {
			continue
		}

		for _, key := range keysFor(word) {
			wordGroup, ok := c.wordGroups[key]
			if !ok {
				// create the new group if it doesn't exist yet
				c.wordGroups[key] = []string{word}
			} else {
				c.wordGroups[key] = append(wordGroup, word)
			}
		}
	}

	return createGraph(c)
}

func createGraph(c *Collection) error {
	// create graph vertices and edges
	for _, wordList := range c.wordGroups {
		for _, word1 := range wordList {
			for _, word2 := range wordList {
				if err := c.graph.AddEdge(word1, word2); err != nil {
					return err
				}
			}
		}
	}
	return nil
}

func keysFor(word string) []string {
	var keys []string
	for i := 0; i < len(word); i++ {
		bWord := []byte(word)
		bWord[i] = '.'
		keys = append(keys, string(bWord))
	}

	return keys
}
