# wordchain

## What is it?

Wordchain is a package with an example command line tool which, given two words 
(start and end) and the dictionary, finds the length of the shortest transformation
sequence from start to end, such that:

- Only one letter can be changed at a time
- Each intermediate word must exist in the given dictionary
- At each step, exactly one character is replaced with another character

## How does it work?

It solves the problem in three steps. First it generates multiple word-groups 
from the dictionary, grouping together words of required length which differ 
only one character. These groups are used to create a graph which is a full mesh
within a given group. Since a word is a single node in the graph, and the same 
word might exist in multiple word-groups, the graph will have edges 'connecting'
the words in separate groups.
  
Breadth first search is performed on the graph starting from our 'start' 
word. When 'end' node is found the path taken by the algorithm is the shortest 
possible transformation between the two words. The path is recorded by using an 
additional field on the nodes 'bestparent' which is the parent node recorded 
at the first visit of the node. Tracking back recursively yields the reverse 
list of visited nodes. This list reversed is the path from 'start' to 'end'. 

## Installation
  
Git and Golang must be installed.  
Golang version >1.6 (should work with all Go versions)
  
To get the code, and build the binary:   
`go get -u gitlab.com/zgiber/wordchain`
  
The package which does the main job will be in $GOPATH/src/gitlab.com/zgiber/wordchain/collection
https://gitlab.com/zgiber/wordchain/tree/master/collection
  
## Testing

`go test ./...`  
or  
`cd $GOPATH/src/gitlab.com/zgiber/wordchain/collection`  
`go test`  
 

## Usage

`wordchain [options] start end`

It is important that `start` and `end` must be of same length. If no dictionary 
is specified in the options, the system default will be tried. If that fails it 
falls back to an embedded english dictionary of ~250K words. The words in the 
dictionary are converted to lower case before they are used. The `start` and 
`end` words don't have to be part of the dictionary.
  
The tool writes the result to the standard output in a JSON formatted array.

```
PARAMETERS:
  start string
	  The starting word for the sequence.
  end string
	  The target word. Must be the same length as start.

OPTIONS:
  -source string
    Path of the source dictionary file to use.
```