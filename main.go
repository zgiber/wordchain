package main

import (
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	"os"

	"gitlab.com/zgiber/wordchain/collection"
	"gitlab.com/zgiber/wordchain/sample"
)

var (
	sourcePath = flag.String("source", "", "Path of the source dictionary file to use.")
)

func init() {
	flag.Usage = printUsage
	flag.Parse()
}

func main() {

	// add start / end
	start, end, err := getArguments()
	if err != nil {
		fmt.Println(err)
		return
	}

	// generate dictionary
	dict, err := sample.GenerateDictionary(len(start), *sourcePath)
	if err != nil {
		fmt.Println(err)
		return
	}

	// adding our words to the dictionary
	// they might be duplicates, but the
	// algorithm properly handles duplicates
	dict = append(dict, start)
	dict = append(dict, end)

	// create our collection and graph
	coll, err := collectionFromDictFile(dict)
	if err != nil {
		fmt.Println(err)
		return
	}

	// perform search
	result, err := collection.BFSTrace(coll, start, end)
	if err != nil {
		fmt.Println(err)
		return
	}

	// json out
	response := map[string]interface{}{
		"length":   len(result),
		"sequence": result,
	}
	err = json.NewEncoder(os.Stdout).Encode(response)
	if err != nil {
		fmt.Println(err)
	}

}

func getArguments() (string, string, error) {
	start := flag.Arg(0)
	end := flag.Arg(1)
	if len(start) != len(end) {
		return "", "", errors.New("start and end word must be of same length")
	}

	if len(start) == 0 {
		return "", "", errors.New("start cannot be emtpy")
	}

	return start, end, nil
}

func collectionFromDictFile(dict []string) (*collection.Collection, error) {
	c := collection.NewCollection()
	if err := c.SetDictionary(dict); err != nil {
		return nil, err
	}
	return c, nil
}

func printUsage() {
	fmt.Print("USAGE:\n  findseq [options] start end\n\n")
	fmt.Print("PARAMETERS:\n")
	fmt.Print("  start string\n\tThe starting word for the sequence.\n")
	fmt.Print("  end string\n\tThe target word. Must be the same length as start.\n")
	fmt.Print("\n")
	fmt.Print("OPTIONS:\n")
	flag.PrintDefaults()
}
