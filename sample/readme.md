# sample
--
    import "github.com/dicdash/sample"


## Usage

EnglishDict is a list of english words separated by '\n' character

#### func  GenerateDictionary

```go
func GenerateDictionary(wordLength int, sourcePath string) ([]string, error)
```
GenerateDictionary takes an existing dictionary file, takes a 'wordCount' number
of words from it which must have 'wordLength' length, and returns them as
[]string. On OSX and Linux if the 'sourcePath' is an empty string, it tries to
use the system dictionary if available.
