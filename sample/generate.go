package sample

import (
	"bufio"
	"bytes"
	"fmt"
	"io"
	"log"
	"os"
	"runtime"
)

// GenerateDictionary takes an existing dictionary file, takes a 'wordCount' number of
// words from it which must have 'wordLength' length, and returns them as []string.
// On OSX and Linux if the 'sourcePath' is an empty string, it tries to use the system
// dictionary if available.
func GenerateDictionary(wordLength int, sourcePath string) ([]string, error) {

	dictLocations := []string{
		"/usr/share/dict/words",
		"/usr/dict/words",
	}

	var source io.Reader
	var err error

	// if path is given, don't try to
	// find os dictionary
	if sourcePath != "" {
		source, err = os.Open(sourcePath)
		if err != nil {
			return nil, err
		}
	}

	// on linux and mac try to use built-in dict.
	if runtime.GOOS == "darwin" || runtime.GOOS == "linux" {
		for _, sourcePath := range dictLocations {
			source, err = os.Open(sourcePath)
			if err == nil {
				break
			}

			fmt.Println(err)
		}
	}

	if source == nil {
		fmt.Println("Fallback to internal english dictionary.")
		source = bytes.NewBuffer(EnglishDict)
	}

	return readAllWords(source, wordLength), nil
}

func readAllWords(input io.Reader, wordLength int) []string {
	nothing := struct{}{} // zero memory placeholder (http://dave.cheney.net/2014/03/25/the-empty-struct)
	reader := bufio.NewReader(input)
	defer func() {
		// closing after read if applicable
		if closer, ok := input.(io.Closer); ok {
			closer.Close()
		}
	}()

	// using map to avoid duplicates
	// also breaks sorting (useful in some cases)
	wordsFound := map[string]struct{}{}
	for {
		line, err := reader.ReadBytes('\n')
		if err != nil {
			if err != io.EOF {
				// quit on any other error than EOF
				log.Fatal(err)
			}
			// EOF means we're done here
			break
		}

		// -1 for counting the '\n'
		if len(line)-1 == wordLength {

			key := bytes.TrimSuffix(line, []byte{'\n'})
			wordsFound[string(key)] = nothing
		}
	}

	result := []string{}
	for k := range wordsFound {
		result = append(result, k)
	}
	return result
}
